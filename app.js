var restify = require('restify');
var builder = require('botbuilder');

//=========================================================
// Bot Setup
//=========================================================

// Setup Restify Server
var server = restify.createServer();
server.listen(80, function () {
	console.log('%s listening to %s', server.name, server.url);
});

//=========================================================
// Create chat bot
//=========================================================
var connector = new builder.ChatConnector({
	appId: 'b08263bb-57b9-4879-9a3e-ae10f7163e51',
	appPassword: 'vncEkpmuo8Xnsb8PHfjg7aj'
});
var bot = new builder.UniversalBot(connector);
server.post('/bot', connector.listen());

//=========================================================
// Create LUIS recognizer that points at our model
// and add it as the root '/' dialog for our Cortana Bot.
//=========================================================
var model = 'https://api.projectoxford.ai/luis/v2.0/apps/e73a92bc-85e7-4400-863e-07e7684ba28b?subscription-key=5e714df24a934663a2b1db30edacc412&verbose=true&q=';
var recognizer = new builder.LuisRecognizer(model);
var dialog = new builder.IntentDialog({ recognizers: [recognizer] });
dialog.onDefault(builder.DialogAction.send("I'm sorry I didn't understand. I can only resend your vouchers."));
//=========================================================
// Bots Dialogs
//=========================================================
bot.dialog('/', dialog);

// Add intent handlers
dialog.matches('ResendVoucher', [
	function (session) {
		session.beginDialog('/askName');
	},
	function (session, results) {
		session.send('Hello %s!', results.response);
	}
]);

bot.dialog('/askName', [
	function (session) {
		builder.Prompts.text(session, 'Hi! What is your name?');
	},
	function (session, results) {
		session.endDialogWithResult(results);
	}
]);